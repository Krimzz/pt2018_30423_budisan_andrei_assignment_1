package edu.ro.utcn.calc.pt.Service;

import edu.ro.utcn.calc.pt.polynomial.calc.Model.Monom;
import edu.ro.utcn.calc.pt.polynomial.calc.Model.Polinom;

public class Operations {

    private Polinom operation = new Polinom();
    private Polinom cpyP1 = new Polinom();
    private Polinom cpyP2 = new Polinom();

    public Polinom addPoli(Polinom p1,Polinom p2){
        operation = new Polinom();

        cpyP1 = p1.copyPolinom();
        cpyP2 = p2.copyPolinom();

        for(Monom m: cpyP1.getPolinom())
            operation.addMonom(m);
        for(Monom m: cpyP2.getPolinom())
            operation.addMonom(m);
        return operation;
    }

    public Polinom subPoli(Polinom p1,Polinom p2){
        cpyP1 = p1.copyPolinom();
        cpyP2 = p2.copyPolinom();

        cpyP2.invertMonom();
        return addPoli(cpyP1,cpyP2);
    }

    public Polinom mulPoli(Polinom p1,Polinom p2){
        operation = new Polinom();

        cpyP1 = p1.copyPolinom();
        cpyP2 = p2.copyPolinom();

        for(Monom m1: cpyP1.getPolinom()){
            for(Monom m2: cpyP2.getPolinom()){
                operation.addMonom(m1.multiplyMonom(m2));
            }
        }

        return operation;
    }

    public Division divPoli(Polinom p1,Polinom p2){
        operation = new Polinom();

        Polinom quotient = new Polinom();
        Polinom remainder = new Polinom();
        Polinom calculation = new Polinom();
        Division division = new Division();

        cpyP1 = p1.copyPolinom();
        cpyP2 = p2.copyPolinom();

        // cpyP1 / cpyP2

        if(cpyP2.getLen()!=0){
            remainder = cpyP1.copyPolinom(); // At each step n = d × q + r
            while (remainder.getLen() != 0 && remainder.getPolinom().get(0).getDegree() >= p2.getPolinom().get(0).getDegree()) {

                calculation.addMonom(new Monom(remainder.getPolinom().get(0).getCoef()/p2.getPolinom().get(0).getCoef(),
                        remainder.getPolinom().get(0).getDegree()-p2.getPolinom().get(0).getDegree()));

                //Divide the leading terms
                quotient = addPoli(quotient, calculation);
                remainder = subPoli(remainder,mulPoli(calculation ,p2));
                calculation.getPolinom().clear();
                // p1 = p1 - impartitor * cat partial
            }
        }
        division.setQuotient(quotient);
        division.setRemainder(remainder);

        return division;  // return class with quotient and remainder as attributes
    }

    public Polinom drvPoli(Polinom p1){
        operation = new Polinom();
        cpyP1 = p1.copyPolinom();

        for(Monom m: cpyP1.getPolinom()){
            operation.addMonom(new Monom(m.getCoef()*m.getDegree(),m.getDegree()-1));
        }

        return operation;
    }

    public Polinom itgPoli(Polinom p1){
        operation = new Polinom();
        cpyP1 = p1.copyPolinom();

        for(Monom m: cpyP1.getPolinom()){
            operation.addMonom(new Monom(m.getCoef()/(m.getDegree()+1),m.getDegree()+1));
        }

        return operation;
    }
}
