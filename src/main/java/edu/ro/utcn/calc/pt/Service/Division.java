package edu.ro.utcn.calc.pt.Service;

import edu.ro.utcn.calc.pt.polynomial.calc.Model.Polinom;

import java.util.Objects;

public class Division {
    private Polinom quotient = new Polinom();
    private Polinom remainder = new Polinom();

    @Override
    public String toString() {
        return "quotient=" + quotient.toString() + "     " + "remainder=" + remainder.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Division division = (Division) o;
        if(this.getQuotient().equals(division.getQuotient()) && this.getRemainder().equals(division.remainder)){
            return true;
        }else {
            return false;
        }
    }


    public Polinom getQuotient() {
        return quotient;
    }

    public void setQuotient(Polinom quotient) {
        this.quotient = quotient;
    }

    public Polinom getRemainder() {
        return remainder;
    }

    public void setRemainder(Polinom remainder) {
        this.remainder = remainder;
    }
}
