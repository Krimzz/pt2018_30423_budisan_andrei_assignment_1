package edu.ro.utcn.calc.pt.polynomial.calc.Model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class Polinom {
    private ArrayList<Monom> polinom = new ArrayList<Monom>();

    public void addMonom(Monom monom){  //adaugare nou monom
        int ok=0;
        for(Monom m: polinom){
            if(m.equals(monom)) {
                m.add(monom);
                ok=1;
            }
        }
        if(ok==0) {
            polinom.add(monom);
        }
        removeAllZeroCoeff();
        sortDescGrad();
    }

    public void removeAllZeroCoeff(){   //remove la monomii cu coeff 0
        polinom.removeIf(term->term.getCoef()==0 || term.getDegree()<0);
    }

    @Override
    public String toString() {          //afisare polinom
        String polinomOut = new String();
        if(polinom == null || getLen()==0){
            polinomOut="0";
            return polinomOut;
        }else {
            for(Monom m:polinom){
                polinomOut = polinomOut.concat(m.toString());
            }
            if (polinom.get(0).getCoef()<0){
                return polinomOut;
            } else {
                return polinomOut.substring(1);
            }
        }
    }

    public void sortDescGrad(){         //sortare descresc
        Collections.sort(polinom);
    }

    public int getLen(){                //lungime polinom
        return polinom.size();
    }

    @Override
    public boolean equals(Object o) {   //daca 2 polinoame sunt egale // pt unit test
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()){
            return false;
        }
        Polinom compare = (Polinom) o;
        if(compare.getLen() != this.getLen()) {
            return false;
        }else{
            for(int i = 0; i < compare.getLen(); i++) {
                if(!this.getPolinom().get(i).equals(compare.getPolinom().get(i))) {
                    return false;
                }
            }
        }
        return true;
    }

    public void invertMonom(){
        for(Monom m: polinom){
            m.setCoef((-1)*m.getCoef());
        }
    }

    public Polinom copyPolinom(){
        Polinom cpyPolinom = new Polinom();
        for(Monom m: polinom){
            cpyPolinom.addMonom(m.copyMonom());
        }
        return cpyPolinom;
    }

    public ArrayList<Monom> getPolinom() {
        return polinom;
    }
}
