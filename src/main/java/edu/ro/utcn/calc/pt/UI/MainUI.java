package edu.ro.utcn.calc.pt.UI;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

public class MainUI extends Application {
    public static void main(String[] args) { launch(args); }
    @Override
    public void start(Stage primaryStage) {
        try{
            Parent root = FXMLLoader.load(getClass().getResource("/fxml/MainUI.fxml"));
            Scene scene = new Scene(root);

            primaryStage.setTitle("Polynomial Calculator");
            primaryStage.setScene(scene);

            primaryStage.setResizable(false);
            primaryStage.setMaximized(false);

            scene.getStylesheets().add(getClass().getResource("/application.css").toExternalForm());

            Image icon = new Image(this.getClass().getResourceAsStream("/Pictures/pheonix-framework.png"));
            primaryStage.getIcons().add(icon);

            primaryStage.setOnCloseRequest(e-> Platform.exit());

            primaryStage.show();
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
