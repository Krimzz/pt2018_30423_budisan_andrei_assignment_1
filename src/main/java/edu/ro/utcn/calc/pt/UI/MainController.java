package edu.ro.utcn.calc.pt.UI;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import edu.ro.utcn.calc.pt.Service.Operations;
import edu.ro.utcn.calc.pt.polynomial.calc.Model.Monom;
import edu.ro.utcn.calc.pt.polynomial.calc.Model.Polinom;
import javafx.fxml.FXML;

import javafx.scene.input.MouseEvent;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MainController{
    @FXML
    private JFXButton addButton;

    @FXML
    private JFXButton subButton;

    @FXML
    private JFXButton mulButton;

    @FXML
    private JFXButton divButton;

    @FXML
    private JFXButton intButton;

    @FXML
    private JFXButton derButton;

    @FXML
    private JFXTextField poli1;

    @FXML
    private JFXTextField poli2;

    @FXML
    private JFXTextField operation;

    @FXML
    public void handleButtonAction(MouseEvent event){
        if (event.getSource() == addButton) {
            Polinom p1;
            Polinom p2;
            p1 = getValueTextField(poli1);
            p2 = getValueTextField(poli2);

            if(p1!=null && p2!=null){
                Operations op = new Operations();
                operation.setText(op.addPoli(p1,p2).toString());
            }
        }

        if (event.getSource() == subButton) {
            Polinom p1;
            Polinom p2;
            p1 = getValueTextField(poli1);
            p2 = getValueTextField(poli2);

            if(p1!=null && p2!=null){
                Operations op = new Operations();
                operation.setText(op.subPoli(p1,p2).toString());
            }
        }

        if (event.getSource() == mulButton) {
            Polinom p1;
            Polinom p2;
            p1 = getValueTextField(poli1);
            p2 = getValueTextField(poli2);

            if(p1!=null && p2!=null){
                Operations op = new Operations();
                operation.setText(op.mulPoli(p1,p2).toString());
            }
        }

        if (event.getSource() == divButton) {
            Polinom p1;
            Polinom p2;
            p1 = getValueTextField(poli1);
            p2 = getValueTextField(poli2);

            if(p1!=null && p2!=null){
                Operations op = new Operations();
                operation.setText(op.divPoli(p1,p2).toString());
            }
        }

        if (event.getSource() == intButton) {
            Polinom p1;
            p1 = getValueTextField(poli1);

            if(p1!=null){
                Operations op = new Operations();
                operation.setText(op.itgPoli(p1).toString());
            }
        }

        if (event.getSource() == derButton) {
            Polinom p1;
            p1 = getValueTextField(poli1);
            if(p1!=null) {
                Operations op = new Operations();
                operation.setText(op.drvPoli(p1).toString());
            }
        }
    }

    private Polinom getValueTextField(JFXTextField p){
        String text = p.getText();
        Polinom polinom = new Polinom();

        text = text.replaceAll("\\s+",""); //delete all empty spaces

        Pattern pattern = Pattern.compile( "((-?\\d+(?:\\.\\d+)?(?=x))?(-?[xX])(\\^(\\d+))?)|(-?\\d+(?:\\.\\d+)?)" );
        Matcher matcher = pattern.matcher(text);

        while (matcher.find()) {
            //CASE: 3x^2, -2x^3
            if ( matcher.group(2) != null && matcher.group(6) == null && matcher.group(5) != null) {
                polinom.addMonom(new Monom(Double.parseDouble(matcher.group(2)), Integer.parseInt(matcher.group(5))));
            }
            //CASE: x^2, -x^3 -> coefficient = 1 OR -1
            if (matcher.group(1) != null && matcher.group(3) != null && matcher.group(4) != null && matcher.group(5) != null) {
                if(matcher.group(3).length() == 1){
                    polinom.addMonom(new Monom(1, Integer.parseInt(matcher.group(5))));
                }else {
                    polinom.addMonom(new Monom(-1, Integer.parseInt(matcher.group(5))));
                }
            }
            //CASE: 3x, -2x -> exponent = 1
            if (matcher.group(2) != null && matcher.group(4) == null && matcher.group(6) == null) {
                polinom.addMonom(new Monom(Double.parseDouble(matcher.group(2)), 1));
            }
            //CASE: x, -x -> coefficient = 1 OR -1, exponent = 1
            if (matcher.group(2) == null && matcher.group(5) == null && matcher.group(6) == null) {
                if(matcher.group(3).length() == 1){
                    polinom.addMonom(new Monom(1, 1));
                }else {
                    polinom.addMonom(new Monom(-1, 1));
                }
            }
            //CASE: 3, -5 -> exponent = 0
            if (matcher.group(6) != null) {
                polinom.addMonom(new Monom(Double.parseDouble(matcher.group(6)), 0));
            }
        }

        if(polinom.getLen() == 0){
            p.getStyleClass().add("invalid-input");
            operation.setText("Invalid input!");
            return null;
        }
        else {
            p.getStyleClass().removeIf(style -> style.equals("invalid-input"));
            return polinom;
        }
    }

}
