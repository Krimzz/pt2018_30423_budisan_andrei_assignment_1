package edu.ro.utcn.calc.pt.polynomial.calc.Model;

public class Monom implements Comparable<Monom>{
    private double coef;
    private int degree;

    @Override
    public String toString() {          //afisare monom
        String monomOut = new String();
        if(degree!=0 && degree!=1) {
            if (coef < 0) { monomOut = format(coef) + "x^" + Integer.toString(degree);
            } else {
                if(coef == 1){ monomOut = "+" + "x^" + Integer.toString(degree);
                }else {
                    if(coef == -1){ monomOut = "-" + "x^" + Integer.toString(degree);
                    }else { monomOut = "+" + format(coef) + "x^" + Integer.toString(degree);
                    }
                }
            }
        }else{
            if(degree == 1){
                if(coef<0){ monomOut = format(coef) + "x";
                }else{
                    if(coef==1){ monomOut = "+" + "x";
                    }else{ monomOut = "+" + format(coef) + "x";
                    }
                }
            }else{
                if (coef < 0) { monomOut = format(coef);
                }else { monomOut = "+" + format(coef);
                }
            }
        }
        return monomOut;
    }

    public boolean equals(Monom monom) { //egalitate grad
        if(this.getDegree()==monom.getDegree()){
            return true;
        }
        return false;
    }

    public void add(Monom monom){        //suma a 2 monoame
        double sumCoef = this.getCoef()+monom.getCoef();
        this.setCoef(sumCoef);
    }

    @Override
    public int compareTo(Monom compareMonom) { //sortare descresc
        int comparedeg = compareMonom.getDegree();
        return comparedeg - this.degree;
    }

    public Monom copyMonom(){
        Monom cpyMonom = new Monom(this.coef,this.degree);
        return cpyMonom;
    }

    public Monom multiplyMonom(Monom m){
        Monom mulMonom = new Monom(this.coef*m.getCoef(),this.degree+m.getDegree());
        return  mulMonom;
    }

    private String format(double number){
        if(number - (int)number != 0){
            return String.format("%.2f",number);
        }else {
            return String.format("%s",(int)number);
        }
    }

    public double getCoef() {
        return coef;
    }

    public void setCoef(double coef) {
        this.coef = coef;
    }

    public int getDegree() {
        return degree;
    }

    public void setDegree(int grad) {
        this.degree = degree;
    }

    public Monom(double coef, int degree) {
        this.coef = coef;
        this.degree = degree;
    }

}
