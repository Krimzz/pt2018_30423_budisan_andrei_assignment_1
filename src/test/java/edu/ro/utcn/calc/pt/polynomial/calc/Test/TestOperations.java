package edu.ro.utcn.calc.pt.polynomial.calc.Test;

import edu.ro.utcn.calc.pt.Service.Division;
import edu.ro.utcn.calc.pt.Service.Operations;
import edu.ro.utcn.calc.pt.polynomial.calc.Model.Monom;
import edu.ro.utcn.calc.pt.polynomial.calc.Model.Polinom;
import static org.junit.Assert.*;
import org.junit.Test;

public class TestOperations {
    @Test
    public void testAddPoli(){
        Polinom p1 = new Polinom();
        p1.addMonom(new Monom(1,2)); //x^2+1
        p1.addMonom(new Monom(1,0)); //x-3

        Polinom p2 = new Polinom();
        p2.addMonom(new Monom(1,1));
        p2.addMonom(new Monom(-3,0));

        Polinom pa;   //actual value
        Operations op = new Operations();

        pa = op.addPoli(p1,p2);

        Polinom pe = new Polinom(); //expected value
        pe.addMonom(new Monom(1,2));
        pe.addMonom(new Monom(1,1));
        pe.addMonom(new Monom(-2,0));

        assertEquals("addition", pe, pa);
    }

    @Test
    public void testSubPoli(){
        Polinom p1 = new Polinom();
        p1.addMonom(new Monom(1,2));
        p1.addMonom(new Monom(1,0));

        Polinom p2 = new Polinom();
        p2.addMonom(new Monom(1,1));
        p2.addMonom(new Monom(-3,0));

        Polinom pa;   //actual value
        Operations op = new Operations();

        pa = op.subPoli(p1,p2);

        Polinom pe = new Polinom(); //expected value
        pe.addMonom(new Monom(1,2));
        pe.addMonom(new Monom(-1,1));
        pe.addMonom(new Monom(4,0));
        assertEquals("subtraction", pe, pa);
    }

    @Test
    public void testMulPoli(){
        Polinom p1 = new Polinom();
        p1.addMonom(new Monom(1,2));
        p1.addMonom(new Monom(1,0));

        Polinom p2 = new Polinom();
        p2.addMonom(new Monom(1,1));
        p2.addMonom(new Monom(-3,0));

        Polinom pa;   //actual value
        Operations op = new Operations();

        pa = op.mulPoli(p1,p2);

        Polinom pe = new Polinom(); //expected value
        pe.addMonom(new Monom(1,3));
        pe.addMonom(new Monom(-3,2));
        pe.addMonom(new Monom(1,1));
        pe.addMonom(new Monom(-3,0));

        assertEquals("multiplication", pe, pa);
    }

    @Test
    public void testDivPoli(){
        Polinom p1 = new Polinom();
        p1.addMonom(new Monom(1,2));
        p1.addMonom(new Monom(1,0));

        Polinom p2 = new Polinom();
        p2.addMonom(new Monom(1,1));
        p2.addMonom(new Monom(-3,0));

        Division pa;   //actual value
        Operations op = new Operations();

        pa = op.divPoli(p1,p2);

        Division pe = new Division(); //expected value
        Polinom quotient = new Polinom();
        Polinom remainder = new Polinom();

        quotient.addMonom(new Monom(1,1));
        quotient.addMonom(new Monom(3,0));
        remainder.addMonom(new Monom(10,0));

        pe.setQuotient(quotient);
        pe.setRemainder(remainder);

        assertEquals("division", pe, pa);
    }

    @Test
    public void testIntPoli(){
        Polinom p1 = new Polinom();
        p1.addMonom(new Monom(1,2));
        p1.addMonom(new Monom(1,0));

        Polinom pa;   //actual value
        Operations op = new Operations();

        pa = op.itgPoli(p1);

        Polinom pe = new Polinom(); //expected value
        pe.addMonom(new Monom(0.33,3));
        pe.addMonom(new Monom(1,1));

        assertEquals("integrate", pe, pa);
    }

    @Test
    public void testDerPoli(){
        Polinom p1 = new Polinom();
        p1.addMonom(new Monom(1,2));
        p1.addMonom(new Monom(1,0));

        Polinom pa;   //actual value
        Operations op = new Operations();

        pa = op.drvPoli(p1);

        Polinom pe = new Polinom(); //expected value
        pe.addMonom(new Monom(2,1));

        assertEquals("derivate", pe, pa);
    }

}
